package br.com.mauda.exemplos.dates.versao7.exemplo001.v1;

import org.junit.Test;

public class Teste { 
	
	@Test
	public void testeOK(){
		ConvertDate.parseDate("08/05/2016", "dd/MM/yyyy");
	}
	
	@Test
	public void testeDataNula(){
		ConvertDate.parseDate(null, "dd/MM/yyyy");
	}

	@Test
	public void testeFormatoNulo(){
		ConvertDate.parseDate("08/05/2016", null);
	}

	@Test
	public void testeDataFormatoDiferentes(){
		ConvertDate.parseDate("08/05/2016", "yyyy/MM/dd");
	}
}